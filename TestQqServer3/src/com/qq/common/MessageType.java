package com.qq.common;
public interface MessageType {
	String messageSucceed="1";//登陆成功
	String messageRegister="2";//注册
	String messageCommes="3";//普通信息包
	String messageGetOnLineFriend="4";//要求在线好友的包
	String messageRetOnLineFriend="5";//返回在线好友的包
	String messageRetFriend="6";//返回用户的好友包
	String messageRetUser="7";//返回所有用户与Id的映射关系
	String messageInquiryFriend="8";//询问加好友包
	String messageGetInquiry="9";//接收询问信息
	String messageClosed="10";//退出
}
