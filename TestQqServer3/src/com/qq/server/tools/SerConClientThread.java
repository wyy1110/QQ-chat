package com.qq.server.tools;
/**
 * 功能：是服务器和某个客户端的通信线程
 */
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashMap;

import com.qq.common.Message;
import com.qq.common.MessageType;
import com.qq.server.db.QQMysql;

public class SerConClientThread  extends Thread{
	private Socket s;

	public Socket getS() {
		return s;
	}

	public void setS(Socket s) {
		this.s = s;
	}

	public SerConClientThread(Socket s) {
		this.s=s;
	}
	public void close() throws IOException {
		s.close();
	}
	//让该线程去通知其它用户
	public void noticeOther(String userId) {
		//得到所有在线的人的线程
		HashMap<String, SerConClientThread> hm= ManageClientThread.hm;
		for (String value : hm.keySet()) {
			if(value.equals(userId)) {
				Message m = new Message();
				m.setMesgText(userId);
				m.setMesType(MessageType.messageRetOnLineFriend);
				//取出在线人的id
				String onLineUserId = value.toString();
				try {
					ObjectOutputStream oos = new ObjectOutputStream(ManageClientThread.getClientThread(onLineUserId).getS().getOutputStream());
					m.setGetterId(onLineUserId);
					oos.writeObject(m);
					oos.flush();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	//向该用户发送好友信息
	public void sengUserFriend(String userId) throws IOException, SQLException {
		SerConClientThread userThread = ManageClientThread.hm.get(userId);
		QQMysql qqMysql = new QQMysql();

		Message m = new Message();
		m.setMesgText(qqMysql.getUserFriend(userId));//好友信息
		System.out.println(qqMysql.getUserFriend(userId));
		m.setMesType(MessageType.messageRetFriend);
		m.setGetterId(userId);

		ObjectOutputStream oos = new ObjectOutputStream(userThread.getS().getOutputStream());
		oos.writeObject(m);
		oos.flush();
	}
	//向客户端发送所有用户与Id的对应关系
	public void sendAllUser(String userId) throws IOException, SQLException {
		SerConClientThread userThread = ManageClientThread.hm.get(userId);
		QQMysql qqMysql = new QQMysql();

		Message m = new Message();
		m.setMesgText(qqMysql.getAllUser());//好友信息
		m.setMesType(MessageType.messageRetUser);
		m.setGetterId(userId);

		ObjectOutputStream oos = new ObjectOutputStream(userThread.getS().getOutputStream());
		oos.writeObject(m);
		oos.flush();
	}
	public void run(){

		while(true) {
			//这里该线程就可以接收客户端的信息.
			try {
				ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
				Message m=(Message)ois.readObject();

				//对从客户端取得的消息进行类型判断，然后做相应的处理
				if(m.getMesType().equals(MessageType.messageCommes)) {//聊天信息
					//取得接收人的通信线程
					System.out.println(m.getSenderId()+" 对 "+m.getGetterId()+" 说:"+m.getMesgText());
					SerConClientThread sc=ManageClientThread.getClientThread(m.getGetterId());
					ObjectOutputStream oos=new ObjectOutputStream(sc.getS().getOutputStream());
					oos.writeObject(m);
					oos.flush();
				}
				else if(m.getMesType().equals(MessageType.messageGetOnLineFriend)) {//要在线好友信息
					System.out.println(m.getSenderId()+" 要他的在线好友");
					//把在服务器的好友给该客户端返回.
					String res=ManageClientThread.getAllOnLineUserid();
					Message m2=new Message();
					m2.setMesType(MessageType.messageRetOnLineFriend);
					m2.setMesgText(res);
					m2.setGetterId(m.getSenderId());
					ObjectOutputStream oos=new ObjectOutputStream(s.getOutputStream());
					oos.writeObject(m2);
					oos.flush();
				}
				else if(m.getMesType().equals(MessageType.messageInquiryFriend)){//询问接收者是否同意添加为好友
					m.setMesType(MessageType.messageGetInquiry);
					SerConClientThread sc=ManageClientThread.getClientThread(m.getGetterId());
					ObjectOutputStream oos=new ObjectOutputStream(sc.getS().getOutputStream());
					oos.writeObject(m);
					oos.flush();
					System.out.println("询问接收者是否同意添加为好友并发送成功");
				}
				else if(m.getMesType().equals(MessageType.messageGetInquiry)){//被加用户返回的数据
					if(m.getMesgText().equals("同意")){//接收者同意请求
						m.setMesType(MessageType.messageInquiryFriend);//发送给请求者请求结果
						SerConClientThread scSend=ManageClientThread.getClientThread(m.getSenderId());
						ObjectOutputStream oos=new ObjectOutputStream(scSend.getS().getOutputStream());
						oos.writeObject(m);
						oos.flush();

						QQMysql qqMysql = new QQMysql();
						qqMysql.updateSQLFriend(m.getSenderId(),m.getGetterId());
						sengUserFriend(m.getGetterId());
						sengUserFriend(m.getSenderId());
					}
					else{
						m.setMesType(MessageType.messageInquiryFriend);
						SerConClientThread scSend=ManageClientThread.getClientThread(m.getSenderId());
						ObjectOutputStream oos=new ObjectOutputStream(scSend.getS().getOutputStream());
						oos.writeObject(m);
						oos.flush();
					}
				}
				else if(m.getMesType().equals(MessageType.messageClosed)){//关闭线程
					SerConClientThread scSend=ManageClientThread.getClientThread(m.getSenderId());
					scSend.close();
					ManageClientThread.removeUserThread(m.getSenderId());
				}
			} catch (Exception e) {
				//throw  new RuntimeException(e);
				e.printStackTrace();
				// TODO: handle exception
			}
		}
	}
}