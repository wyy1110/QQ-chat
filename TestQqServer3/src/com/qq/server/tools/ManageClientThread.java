package com.qq.client.tools;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.qq.client.view.QqChat;
import com.qq.common.Message;

import com.qq.common.MessageType;
import com.qq.common.User;

import javax.swing.*;


public class ClientConServerThread extends Thread {
	private Socket s;
	//构造函数
	public ClientConServerThread(Socket s){
		this.s=s;
	}
	public void getFriendList() throws IOException, ClassNotFoundException {
		ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
		Message m=(Message)ois.readObject();
		//ManageQqFriendList qqFriendList = new ManageQqFriendList();
		String mesgText = (String) m.getMesgText();
		String getterId = m.getGetterId();

		if(m.getMesType().equals(MessageType.message_ret_Friend)&&mesgText!=null) {
			String[] friend = mesgText.split(",");
			List<User> userFriends = new ArrayList<>();
			for (String value : friend) {
				userFriends.add(ManageQqFriendList.userMap.get(value));
			}
			//添加到好友列表中
			ManageQqFriendList.friendMap.put(getterId, userFriends);
		}
		//ManageQqFriendList.friendMap.put(getterId, null);
	}
	public void getAllUser() throws IOException, ClassNotFoundException {
		//messageRetUser
		ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
		Message m=(Message)ois.readObject();
		//ManageQqFriendList qqFriendList = new ManageQqFriendList();
		if(m.getMesType().equals("7")) {
			ManageQqFriendList.userMap=(HashMap<String, User>) m.getMesgText();
		}
	}
	public void inquiryServer(String userId,String getterId) throws IOException {//向服务器询问添加好友的信息
		System.out.println(userId+","+getterId);
		Message m=new Message();
		m.setSenderId(userId);
		m.setGetterId(getterId);
		m.setMesgText(userId+"想要添加您为好友");
		m.setMesType("10");
		//ClientConServerThread cost = ManageClientConServerThread.getClientConServerThread(userId);
		ObjectOutputStream oos=new ObjectOutputStream(this.s.getOutputStream());
		oos.writeObject(m);
		oos.flush();
	}
//	public boolean getInquiryAns() throws IOException, ClassNotFoundException {
//		ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
//		Message m=(Message)ois.readObject();
//		if(m.getMesgText().equals("同意")){
//			JOptionPane.showMessageDialog(null, "对方同意添加好友啦！快去聊天吧~");
//			return true;
//		}else{
//			JOptionPane.showMessageDialog(null, "对方未同意您的请求");
//			return false;
//		}
//	}
	public void test(Message m) throws IOException {
		int result=-1;
		result = JOptionPane.showConfirmDialog(null, m.getSenderId() + "想添加您为好友", "加好友", JOptionPane.YES_NO_OPTION);
		if (result == 0) m.setMesgText("同意");
		else m.setMesgText("不同意");
		System.out.println(m.getMesgText());
		ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
		oos.writeObject(m);
		oos.flush();
	}
	public void run(){
		while(true){
			//不停的读取从服务器端发来的消息
			try {
				ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
				Message m=(Message)ois.readObject();
				ManageQqFriendList qqFriendList = new ManageQqFriendList();
				switch (m.getMesType()) {
					case "3"://普通信息包
						//把从服务器获得消息，显示到该显示的聊天界面
						QqChat qqChat = new ManageQqChat().getQqChat(m.getGetterId() + " " + m.getSenderId());
						qqChat.showMessage(m);//显示
						break;
					case "5": //在线用户包
						String userOnLine = (String) m.getMesgText();

						break;
					case "6": //接收到好友包
						String mesgText = (String) m.getMesgText();
						String getterId = m.getGetterId();
						if (mesgText != null) {
							String[] friend = mesgText.split(",");
							List<User> userFriends = new ArrayList<>();
							for (String value : friend) {
								userFriends.add(qqFriendList.getUserMap().get(value));
							}
							//添加到好友列表中
							qqFriendList.setFriendMap(getterId, userFriends);
						}
						qqFriendList.setFriendMap(getterId, null);
						break;
					case "7"://返回所有用户与Id的映射关系
						qqFriendList.setUserMap((HashMap<String, User>) m.getMesgText());
						break;
					case "10"://添加好友询问
						if (m.getMesgText().equals("同意")) {
							JOptionPane.showMessageDialog(null, "对方同意添加好友啦！快去聊天吧~");
							User friend = ManageQqFriendList.userMap.get(m.getGetterId());
							ManageQqFriendList.addFriend(m.getSenderId(), friend);
						} else {
							JOptionPane.showMessageDialog(null, "对方未同意您的请求");
						}
						break;
					case "11"://接收好友询问的信息
						Message me=new Message();
						me.setMesType("11");
						me.setGetterId(m.getGetterId());
						me.setSenderId(m.getSenderId());
						int option=JOptionPane.YES_NO_OPTION;
						option = JOptionPane.showConfirmDialog(null, m.getSenderId() + "想添加您为好友", "加好友", JOptionPane.YES_NO_OPTION);
						if (option == JOptionPane.YES_OPTION){
							me.setMesgText("同意");
						}
						else me.setMesgText("不同意");
						ClientConServerThread cost = ManageClientConServerThread.getClientConServerThread(m.getGetterId());
						ObjectOutputStream oos = new ObjectOutputStream(cost.getS().getOutputStream());
						oos.writeObject(me);
						oos.flush();
						System.out.println("接受者已经将结果发送给服务器");
						//test(m);
						break;
					default:
						throw new IllegalStateException("Unexpected value: " + m.getMesType());
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		public void updateFriend(User user){
			Message m=new Message();
			m.setSenderId(user.getUserId());
			m.setMesgText(ManageQqFriendList.friendMap);
			m.setMesType("8");
		}
		public Socket getS() {
			return s;
		}

		public void setS(Socket s) {
			this.s = s;
		}
		
	}