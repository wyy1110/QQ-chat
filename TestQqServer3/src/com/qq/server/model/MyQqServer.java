package com.qq.server.moddel;
/**
 * 这是qq服务器，它在监听，等待某个qq客户端，来连接
 */


import com.qq.common.Message;
import com.qq.common.MessageType;
import com.qq.common.User;
import com.qq.server.db.QQMysql;
import com.qq.server.tools.ManageClientThread;
import com.qq.server.tools.SerConClientThread;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class MyQqServer {
	public MyQqServer() {
		try {
			//在8080监听
			System.out.println("我是服务器，在8080监听");
			ServerSocket ss=new ServerSocket(8080);
			//阻塞,等待连接
			while(true) {
				Socket s=ss.accept();
				//接收客户端发来的信息.
				ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
				User u= (User) ois.readObject();

				QQMysql qqMysql = new QQMysql();//连接数据库，校验密码
				ObjectOutputStream oos=new ObjectOutputStream(s.getOutputStream());
				Message me=new Message();

				if(u.getUserName()==null) {//登录

					User user = qqMysql.findUser(u);
					String id=user.getUserId();
					if (user.getUserName()!=null) {
						me.setMesType(MessageType.messageSucceed);
						me.setMesgText(user);
						oos.writeObject(me);
						oos.flush();

						SerConClientThread scct = new SerConClientThread(s);
						ManageClientThread.addClientThread(u.getUserId(), scct);
						scct.sendAllUser(u.getUserId());//发送所有用户信息
						scct.sengUserFriend(u.getUserId());//发送该用户的好友信息
						//启动与该客户端通信的线程.
						scct.noticeOther(u.getUserId());//通知其它在线用户.
						scct.start();
					}else {
						user = null;
						me.setMesType(MessageType.messageSucceed);
						me.setMesgText(user);
						oos.writeObject(me);
						oos.flush();
					}
				}else{//注册
					if(qqMysql.addUser(u)){
						me.setMesType(MessageType.messageRegister);
						me.setMesgText("成功");
						oos.writeObject(me);
						oos.flush();
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
