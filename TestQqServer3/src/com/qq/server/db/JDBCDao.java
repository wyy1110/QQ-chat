package com.qq.server.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class JDBCUtil {
    private static final String user;//QQ号
    private static final String password;//密码
    private static final String url;//url
    private static final String driver;//驱动

    static {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("src\\mysql.properties"));

            user = properties.getProperty("user");
            password = properties.getProperty("password");
            url = properties.getProperty("url");
            driver = properties.getProperty("driver");
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    }
    //建立连接
    public  static Connection getConnection(){
        try {
            return DriverManager.getConnection(url,user,password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 关闭相关资源
     * @param set
     * @param statement
     * @param connection
     * 不为空就关闭
     */

    public static void close(ResultSet set, Statement statement,Connection connection){
        //判断是否为空
            try {
                if(set!=null) set.close();
                if(statement!=null)statement.close();
                if(connection!=null)connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
    }
}
