package com.qq.server.db;



import com.qq.common.User;

import java.sql.*;
import java.util.HashMap;


public class QQMysql implements JDBCDao {
    private Connection connect;
    public Connection getConnect() {
        return connect;
    }
    public void setConnect(Connection connect) {
        this.connect = connect;
    }
    public  QQMysql() {
        this.connect=JDBCUtil.getConnection();
    }
    public User findUser(User u ) throws SQLException {//查找用户
        boolean b=false;
        User user=new User();
        Statement statement = this.connect.createStatement();
        String sql = "select id,name,password,gender,headImage,statue,friend from usermessage";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String userId = resultSet.getString(1);
            String name=resultSet.getString(2);
            String userPassword = resultSet.getString(3);

            if (u.getUserId().equals(userId) && u.getPasswd().equals(userPassword)) {
                u.setUserId(u.getUserId());
                u.setUserName(name);
                u.setPasswd(userPassword);
                u.setGender(resultSet.getString(4));
                u.setHeadImage(resultSet.getString(5));
                u.setStatue(resultSet.getInt(6));
                u.setFriendId(resultSet.getString(7));
                //b=true;
            }
        }
        JDBCUtil.close(resultSet,statement,null);
       return u;
    }
    public boolean addUser(User u) throws SQLException {//添加用户
        boolean b=true;
        String sql = "insert into usermessage(id,name,password,gender,headImage,statue,friend) values (?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement = this.connect.prepareStatement(sql);
        preparedStatement.setString(1,u.getUserId());
        preparedStatement.setString(2,u.getUserName());
        preparedStatement.setString(3,u.getPasswd());
        preparedStatement.setString(4,u.getGender());
        preparedStatement.setString(5, u.getHeadImage());
        preparedStatement.setInt(6, u.getStatue());
        preparedStatement.setString(7 ,u.getFriendId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return b;
    }
    public void changeUser(User u) throws SQLException {
        String sql="update usermessage set friend = "+u.getFriendId()+" where id = "+u.getUserId();
        Statement statement = this.connect.createStatement();
        statement.executeUpdate(sql);
        JDBCUtil.close(null,statement,null);
    }
    public void updateSQLFriend(String userId,String friendId) throws SQLException {
        System.out.println(userId+","+friendId);
        String userFriend = getUserFriend(userId);//用户的好友列表
        String friend = getUserFriend(friendId);//好友的好友表
        String friendList=null;

        if(userFriend!=null)friendList=userFriend+","+friendId;
        else friendList=friendId;
        System.out.println(userId+friendList);
        String sql="update usermessage set friend = '"+friendList+"' where id = '"+userId+"'";
        Statement statement = this.connect.createStatement();
        statement.executeUpdate(sql);
        System.out.println("数据库写入成功");

        if(friend!=null)friendList=friend+","+userId;
        else friendList=userId;
        System.out.println(friendId+friendList);
        String sql1="update usermessage set friend = '"+friendList+"' where id = '"+friendId+"'";
        statement.executeUpdate(sql1);
        JDBCUtil.close(null,statement,null);
        System.out.println("数据库写入成功");
    }
    public void close(){
       JDBCUtil.close(null,null,this.connect);
    }
    @Override
    public String getUserFriend(String userId) throws SQLException {
        String friends = null;
        Statement statement = this.connect.createStatement();
        String sql = "select id,friend from usermessage";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String id = resultSet.getString(1);
           if(id.equals(userId)){
               friends=resultSet.getString(2);
               System.out.println(friends);
           }
        }
        JDBCUtil.close(resultSet,statement,null);
        return friends;
    }
    public HashMap<String,User> getAllUser() throws SQLException {//获取用户的id与User的映射关系
        HashMap<String,User> userHashMap=new HashMap<>();

        Statement statement = this.connect.createStatement();
        String sql = "select id,name,password,gender,headImage,statue,friend from usermessage";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String id=resultSet.getString(1);
            User user=new User(id,resultSet.getString(2),resultSet.getString(3),
                    resultSet.getString(4),resultSet.getString(5),resultSet.getInt(6),resultSet.getString(7));
            userHashMap.put(id,user);
        }
        JDBCUtil.close(resultSet,statement,null);
        return userHashMap;
    }
}
