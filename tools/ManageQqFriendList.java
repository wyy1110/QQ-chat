package com.qq.client.tools;

import com.qq.common.Message;
import com.qq.common.User;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;

public class ManageQqFriendList {
	public  static HashMap<String,List<User>> friendMap=new HashMap<>();//存储用户好友
	public static HashMap<String,User> userMap;//用户与id的对应

	public void setFriendMap(String userId,List<User> list) {
		friendMap.put(userId,list);

	}

	public  HashMap<String, User> getUserMap() {
		return userMap;
	}

	public void setUserMap(HashMap<String,User> users){
		userMap=users;
	}
	public static List<User> getList(String id) {//得到好友列表
		if(friendMap!=null) return friendMap.get(id);
		return null;
	}

	public static void addFriend(String id,User friend) {//添加好友到列表
		if (friendMap == null) {
			HashMap<String, List<User>> friendHash=new HashMap<>();
			List<User> friendList = new ArrayList<>();
			friendList.add(friend);
			friendHash.put(id, friendList);
			friendMap=friendHash;

		} else {
			List<User> friendList = friendMap.get(id);
			if(friendList!=null) {
				friendList.add(friend);
				friendMap.put(id, friendList);
			}
			else{
				friendMap.put(id, null);
			}
		}
	}

}
