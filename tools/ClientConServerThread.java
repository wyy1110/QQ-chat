package com.qq.client.tools;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.qq.client.view.Chat;
import com.qq.client.view.FriendList;
import com.qq.common.Message;

import com.qq.common.MessageType;
import com.qq.common.User;

import javax.swing.*;


public class ClientConServerThread extends Thread {
	private Socket s;
	public Socket getS() {
		return s;
	}
	public void setS(Socket s) {
		this.s = s;
	}

	//构造函数
	public ClientConServerThread(Socket s){
		this.s=s;
	}
	public void getFriendList() throws IOException, ClassNotFoundException {
		ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
		Message m=(Message)ois.readObject();
		//ManageQqFriendList qqFriendList = new ManageQqFriendList();
		String mesgText = (String) m.getMesgText();
		String getterId = m.getGetterId();
		System.out.println("服务器"+mesgText);
		if(m.getMesType().equals(MessageType.messageRetFriend)&&mesgText!=null) {
			String[] friend = mesgText.split(",");
			System.out.println(friend);
			List<User> userFriends = new ArrayList<>();
			for (String value : friend) {
				System.out.println("好友Id"+value);
				userFriends.add(ManageQqFriendList.userMap.get(value));
				System.out.println(ManageQqFriendList.userMap.get(value).toString());
			}
			//添加到好友列表中
			ManageQqFriendList.friendMap.put(getterId, userFriends);
			System.out.println(ManageQqFriendList.friendMap.get(getterId));
		}
		//ManageQqFriendList.friendMap.put(getterId, null);
	}
	public void getAllUser() throws IOException, ClassNotFoundException {
		//messageRetUser
		ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
		Message m=(Message)ois.readObject();
		//ManageQqFriendList qqFriendList = new ManageQqFriendList();
		if(m.getMesType().equals(MessageType.messageRetUser)) {
			ManageQqFriendList.userMap=(HashMap<String, User>) m.getMesgText();
		}
	}
	public void inquiryServer(String userId,String getterId) throws IOException {//向服务器询问添加好友的信息
		Message m=new Message();
		m.setSenderId(userId);
		m.setGetterId(getterId);
		m.setMesgText(userId+"想要添加您为好友");
		m.setMesType(MessageType.messageInquiryFriend);
		//ClientConServerThread cost = ManageClientConServerThread.getClientConServerThread(userId);
		ObjectOutputStream oos=new ObjectOutputStream(this.s.getOutputStream());
		oos.writeObject(m);
		oos.flush();
		System.out.println("用户已经向服务器发送询问请求");
	}

	public void test(Message m) throws IOException {
		int result=-1;
		result = JOptionPane.showConfirmDialog(null, m.getSenderId() + "想添加您为好友", "加好友", JOptionPane.YES_NO_OPTION);
		if (result == 0) m.setMesgText("同意");
		else m.setMesgText("不同意");
		System.out.println(m.getMesgText());
		ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
		oos.writeObject(m);
		oos.flush();
	}
	public void run(){
		while(true){
			//不停的读取从服务器端发来的消息
			try {
				ObjectInputStream ois=new ObjectInputStream(s.getInputStream());
				Message m=(Message)ois.readObject();
				ManageQqFriendList qqFriendList = new ManageQqFriendList();

				switch (m.getMesType()) {
					case MessageType.messageCommes://普通信息包
						//把从服务器获得消息，显示到该显示的聊天界面
						Chat.showMessage(m);//显示
						break;
					case MessageType.messageRetOnLineFriend: //在线用户包
						String userOnLine = (String) m.getMesgText();

						break;
					case MessageType.messageRetFriend: //接收到好友包
						String mesgText = (String) m.getMesgText();
						String getterId = m.getGetterId();
						if (mesgText != null) {
							String[] friend = mesgText.split(",");
							List<User> userFriends = new ArrayList<>();
							for (String value : friend) {
								userFriends.add(qqFriendList.getUserMap().get(value));
							}
							//添加到好友列表中
							qqFriendList.setFriendMap(getterId, userFriends);
							FriendList.upateFriend(qqFriendList.getUserMap().get(getterId));
						}
						else qqFriendList.setFriendMap(getterId, null);
						break;
					case MessageType.messageRetUser://返回所有用户与Id的映射关系
						qqFriendList.setUserMap((HashMap<String, User>) m.getMesgText());
						break;
					case MessageType.messageInquiryFriend://8添加好友询问
						System.out.println("添加者得到服务器返回的结果");
						if (m.getMesgText().equals("同意")) {
							JOptionPane.showMessageDialog(null, "对方同意添加好友啦！快去聊天吧~");
							System.out.println("通过了好友验证");
						} else {
							JOptionPane.showMessageDialog(null, "对方未同意您的请求");
						}
						break;
					case MessageType.messageGetInquiry://9接收好友询问的信息
						Message me=new Message();
						me.setMesType(MessageType.messageGetInquiry);
						me.setGetterId(m.getGetterId());
						me.setSenderId(m.getSenderId());

						int option=JOptionPane.YES_NO_OPTION;
						option = JOptionPane.showConfirmDialog(null, m.getSenderId() + "想添加您为好友", "加好友", JOptionPane.YES_NO_OPTION);
						if (option == JOptionPane.YES_OPTION){
							me.setMesgText("同意");
						}
						else me.setMesgText("不同意");
						ClientConServerThread cost = ManageClientConServerThread.getClientConServerThread(m.getGetterId());
						ObjectOutputStream oos = new ObjectOutputStream(cost.getS().getOutputStream());
						oos.writeObject(me);
						oos.flush();
						//test(m);
						break;
					default:
						throw new IllegalStateException("Unexpected value: " + m.getMesType());
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}


	}