package com.qq.common;

public class Message implements java.io.Serializable {
	private static final long serialVersionUID = 42L;
	private String mesType;//信息类型
	private String senderId;//发送者Id
	private String getterId;//接受者的Id
	private Object mesgText;//信息内容
	private String sendTime;//发送时间

	public String getMesType() {
		return mesType;
	}

	public void setMesType(String mesType) {
		this.mesType = mesType;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getGetterId() {
		return getterId;
	}

	public void setGetterId(String getterId) {
		this.getterId = getterId;
	}

	public Object getMesgText() {
		return mesgText;
	}

	public void setMesgText(Object mesgText) {
		this.mesgText = mesgText;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
}