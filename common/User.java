package com.qq.common;

public class User implements java.io.Serializable {
	private static final long serialVersionUID = 42L;
	private String userId;
	private String userName;
	private String passwd;
	private String gender;
	private String headImage;
	private int statue;
	private String friendId;

	public User() {
	}

	public User(String userId, String userName, String passwd, String gender, String headImage, int statue, String friendId) {
		this.userId = userId;
		this.userName = userName;
		this.passwd = passwd;
		this.gender = gender;
		this.headImage = headImage;
		this.statue = statue;
		this.friendId = friendId;
	}



	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHeadImage() {
		return headImage;
	}

	public void setHeadImage(String headImage) {
		this.headImage = headImage;
	}

	public int getStatue() {
		return statue;
	}

	public void setStatue(int statue) {
		this.statue = statue;
	}

	public String getFriendId() {
		return friendId;
	}

	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}
}