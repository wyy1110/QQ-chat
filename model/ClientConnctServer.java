package com.qq.client.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.qq.common.Message;

import com.qq.client.tools.ClientConServerThread;
import com.qq.client.tools.ManageClientConServerThread;
import com.qq.common.MessageType;
import com.qq.common.User;


/**
 * 客户端与服务器连接
 * @author wyy
 *
 */
public class ClientConnctServer {
	private Socket socket;
	public Socket getSocket() {
		return socket;
	}
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public boolean sendRegisterToServer(User user) throws IOException, ClassNotFoundException {//发送注册请求
		boolean b=false;

		ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
		oos.writeObject(user);
		oos.flush();
		//接收数据
		ObjectInputStream ois=new ObjectInputStream(socket.getInputStream());
		Message ms=(Message)ois.readObject();
		if(ms.getMesgText().equals("成功")){
			b=true;
		}
		return b;
	}
	//发送请求
	public User sendLoginInfoToServer(Object o) {
		User user = null;
		try {
			//向服务器发送信息
			ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(o);
			oos.flush();

			//接收数据
			ObjectInputStream ois=new ObjectInputStream(this.socket.getInputStream());
			Message ms=(Message)ois.readObject();
			user=(User) ms.getMesgText();
			//用户登录
			if(ms.getMesType().equals(MessageType.messageSucceed)) {//登录成功，就创建一个线程
				//创建一个qq
				ClientConServerThread cost=new ClientConServerThread(this.socket);
				cost.getAllUser();
				cost.getFriendList();
				cost.start();
				ManageClientConServerThread.addClientConServerThread(((User)o).getUserId(),cost);//将用户的线程存入hashma
			}
			else {
				this.socket.close();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	public ClientConnctServer() {
		try {
			this.socket=new Socket("192.168.43.235",8080);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
